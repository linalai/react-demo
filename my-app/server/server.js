const express = require('express')
const mongoose = require('mongoose')

//链接monge 并且使用imooc这个集合
const DB_URL = 'mongodb://127.0.0.1:27017/imooc' //  mongodb://localhost:27017
mongoose.connect(DB_URL)
mongoose.connection.on('connected', function(){
    console.log('mogo connect success')
})
//类似于mysql的 mongo里有文档、字段概念
const User = mongoose.model('user', new mongoose.Schema({
    user: {type: String, required: true},
    age:{type: Number, required: true}
}))
// 新增数据
// User.create({
//     user: "xiaohua",
//     age: 26
// }, function(err, doc){
//     if(!err) {
//         console.log(doc)
//     }else{
//         console.log(err)
//     }
// })

//删除数据
// User.remove({age: 18}, function(err, doc){
//     if(!err){
//         console.log('delete success')
//         User.find({}, function(err, doc) {
//             console.log(doc)
//         })
//     }
//     console.log(doc)
// })

//更新数据
User.update({'user':'xiaoming'}, {'$set':{age: 27}}, function(err, doc){
    console.log(doc)
}) 

//新建app
const app = express()

app.get('/', function (req, res) {
    res.send('<h1>Hellow world ！</h1>')
})

app.get('/data', function (req, res) {
    //查找数据
    User.findOne({'user': 'xiaoming'}, function(err, doc){
        res.json(doc)
    })
    // res.json({
    //     name: 'imooc----test',
    //     type: 'It'
    // })
})
app.listen(9093, function () {
    console.log("Node app start at port 9093")
})