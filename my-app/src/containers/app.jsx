import React from 'react';
import { connect } from 'react-redux';

class AppContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            loadingState: true
        }
    }
    componentDidMount(){
        this.setState({
            loadingState: false
        })
    }
    render() {
        return (
            <div>{this.state.loadingState ? <div>正在加载loading...</div> : this.props.children}</div>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch){
    return{}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppContainer)