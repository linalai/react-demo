import React from 'react';
import Counter from '../components/counter'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {addCount, removeCount} from '../actions/counter'

class CounterContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    render() {
        return (
            <Counter {...this.props}/>
        )
    }
}

function mapStateToProps(state) {
    console.log('========state', state)
    return {
        value: state.counter
    }
}

function mapDispatchToProps(dispatch) {
    console.log('========state', dispatch)
    return {
        // addCount: () => bindActionCreators(addCount, dispatch)
        addCount: () => dispatch(addCount())
        // removeCount: () => dispatch(removeCount)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Counter)