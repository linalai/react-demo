import React from 'react'
import { connect } from 'react-redux'
import UserInfo from '../components/userInfo/userinfo'


class User extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    componentDidMount() {
        // console.log('-----44444', this.props)
    }

    render() {
        return (
            <UserInfo {...this.props} />
        )
    }
}
function mapStateToProps(state) {
    console.log('-----mapStateToProps', state)
    return {
        userinfo: state.userinfo
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User)