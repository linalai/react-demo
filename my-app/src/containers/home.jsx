import React from 'react';
import { connect } from 'react-redux';
import Home from '../components/home';

class HomeContainer extends React.Component {
    constructor() {
        super();
        this.state = {}
    }
    render() {
        return (
            <Home />
        )
    }
}

function mapStateToProps(state) {
    return {
        HomeMsg: state
    }
}

function mapDispatchToProps(dispatch){
    return{    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)