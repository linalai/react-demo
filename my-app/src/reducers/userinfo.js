import * as actionType from '../constants/userinfo'

const initialState = {}

export default function userinfo(state = initialState, action) {
    console.log('render-----', action.type, actionType)
    switch (action.type) {
        case actionType.USERINFO_UPDATE:
            return action.data;
        default:
            return Object.assign({username: 'username'});
    }
}