/*
 * @Author: lailina 
 * @Date: 2019-01-30 15:57:56 
 * @Last Modified by: lailina
 * @Last Modified time: 2019-02-01 18:14:14
 */
import {combineReducers} from 'redux'
// import userinfo from './userinfo'

function counter(state = { count: 0 }, action) {
    // const count = state.counter
    console.log('99999', action.type)
    switch (action.type) {
      case 'ADD_COUNT':
        return state + 1
      default:
        return 50
    }
  }
  
const rootReducer = combineReducers({
    counter
    // userinfo
})

export default rootReducer