import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    console.log('=====', this.props.strore)
    const store = this.props.strore
    const num = store.getState()
    const addCount = this.props.addCount
    const removeCount = this.props.removeCount
    return (
      <div className="App">
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header> */}
        <h1>现在计数器：{num}</h1>
        <button onClick={()=>store.dispatch(addCount())}>计数器相加</button>
        <button onClick={()=>store.dispatch(removeCount())}>计数器相减</button>
      </div>
    );
  }
}

export default App;
