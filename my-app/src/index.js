/*
 * @Author: lailina 
 * @Date: 2019-01-30 15:46:50 
 * @Last Modified by: lailina
 * @Last Modified time: 2019-02-01 17:21:40
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { hashHistory } from 'react-router'
import {Provider} from 'react-redux'
import configureStore from './strore/configureStore'
// import App from './App';
// import * as serviceWorker from './serviceWorker';
import RouteMap from './router/routerMap'


const store = configureStore()

ReactDOM.render( 
    <Provider store={store}>
        <div>
            <RouteMap history={hashHistory} />
        </div>
    </Provider>,
  document.getElementById('root')
);

// import React from 'react';
// import ReactDOM from 'react-dom';
// import {createStore} from 'redux'
// import {counter, addCount, removeCount} from './index.redux'
// import App from './App'
// const store = createStore(counter)

// function render() {
//     console.log('-------strore', store.getState())
//     ReactDOM.render( <App strore = {store} addCount={addCount} removeCount={removeCount}/>,document.getElementById('root'))
// }
// render()
// store.subscribe(render)
