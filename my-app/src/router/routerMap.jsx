import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import HomeContainer from '../containers/home';
import DetailContainer from '../containers/detail'
import AppContainer from '../containers/app'
import CounterContainer from '../containers/counter'
import User from '../containers/userinfo'

class RouteMap extends React.Component {
    render() {
        return (
            <Router history={this.props.history}>
               <Route path="/" component={AppContainer}>
                    <IndexRoute component={CounterContainer} />>
                    <Route path="/home" component={HomeContainer}></Route>
                    <Route path="/detail" component={DetailContainer}></Route>
                    <Route path="/user" component={User}/>
                </Route>
            </Router>
        )
    }
}
export default RouteMap