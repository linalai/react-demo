const ADD_COUNT = '加计数器'
const REMOVE_COUNT = '减计数器'

//reducer
export function counter(state = 0, action) {
    switch (action.type) {
        case ADD_COUNT:
            return state + 1;
        case REMOVE_COUNT:
            return state - 1;
        default:
            return 10
    }
}


export function addCount() {
    return {
        type: ADD_COUNT
    }
}

export function removeCount() {
    return {
        type: REMOVE_COUNT
    }
}