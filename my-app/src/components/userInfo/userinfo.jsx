import React from 'react'
import './userInfo.scss';

export default class UserInfo extends React.Component{
    constructor(props){
        super(props);
        this.state = {}
    }
    render() {
        return(
            <div className="userinfo-container">
                <p>
                    <i className="icon-user"></i>
                    &nbsp;
                    <span>{this.props.userinfo.username}</span>
                </p>
                <p>
                    <i className="icon-map-marker"></i>
                    &nbsp;
                    <span>{this.props.city}</span>
                </p>
            </div>
        )
    }
}