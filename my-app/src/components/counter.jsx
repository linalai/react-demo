import React from 'react';

export default class Counter extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    addCount(){
        this.props.addCount()
        console.log('----this.props.addCount()', this.props.addCount())
    }
    render() {
        console.log('-----', this.props)
        const {value, addCount, removeCount} = this.props
        return(
            <div>
                <span>{value}</span>
                <button onClick={this.addCount.bind(this)}>点击+1</button>
            </div>
        )
    }
}